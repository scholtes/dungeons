package me.scholtes.dungeons.commands;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import io.lumine.xikage.mythicmobs.MythicMobs;
import io.lumine.xikage.mythicmobs.api.bukkit.BukkitAPIHelper;
import io.lumine.xikage.mythicmobs.api.exceptions.InvalidMobTypeException;
import me.scholtes.dungeons.Dungeons;
import me.scholtes.dungeons.Utils;

public class BossKillCMD implements CommandExecutor {

    private Utils utils;
    private Dungeons plugin;
    private BukkitAPIHelper mm = MythicMobs.inst().getAPIHelper();
	
	public BossKillCMD(Utils utils, Dungeons plugin) {
		this.utils = utils;
		this.plugin = plugin;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		File dungeons = new File("plugins/Dungeons/", "dungeons.yml");
		FileConfiguration dungeon = YamlConfiguration.loadConfiguration(dungeons);
		
		if (!sender.isOp()) {
			sender.sendMessage(utils.color("&cYou cannot do this!"));
			return true;
		}
		if (args.length == 0) {
			sender.sendMessage(utils.color("&cCorrect usage: /dungeonkill <dungeon>"));
			return true;
		}
		boolean isActive = false;
		for (String key : dungeon.getConfigurationSection("dungeons").getKeys(false)) {
			if (key.equals(args[0])) {
				isActive = true;
				break;
			}
			if (args[0].contains("Z")) {
				if (key.equals("Z")) {
					isActive = true;
					break;
				}
			}
		}
		if (!isActive) {
			sender.sendMessage(utils.color("&cThat dungeon isn't active"));
			return true;
		}
		
		
		if (args[0].equals("AZ")) {
			spawnZBoss("BZ");
			return true;
		}
		if (args[0].equals("BZ")) {
			spawnZBoss("CZ");
			return true;
		}
		if (args[0].equals("CZ")) {
			spawnZBoss("DZ");
			return true;
		}
		if (args[0].equals("DZ")) {
			spawnZBoss("EZ");
			return true;
		}
		if (args[0].equals("EZ")) {
			spawnZBoss("FZ");
			return true;
		}
		if (args[0].equals("FZ")) {
			spawnZBoss("GZ");
			return true;
		}
		if (args[0].equals("GZ")) {
			spawnZBoss("HZ");
			return true;
		}
		if (args[0].equals("HZ")) {
			spawnZBoss("IZ");
			return true;
		}
		if (args[0].equals("IZ")) {
			spawnZBoss("JZ");
			return true;
		}
		if (args[0].equals("JZ")) {
			spawnZBoss("KZ");
			return true;
		}
		if (args[0].equals("KZ")) {
			spawnZBoss("LZ");
			return true;
		}
		if (args[0].equals("LZ")) {
			spawnZBoss("MZ");
			return true;
		}
		if (args[0].equals("MZ")) {
			spawnZBoss("NZ");
			return true;
		}
		if (args[0].equals("NZ")) {
			spawnZBoss("OZ");
			return true;
		}
		if (args[0].equals("OZ")) {
			spawnZBoss("PZ");
			return true;
		}
		if (args[0].equals("PZ")) {
			spawnZBoss("QZ");
			return true;
		}
		if (args[0].equals("QZ")) {
			spawnZBoss("RZ");
			return true;
		}
		if (args[0].equals("RZ")) {
			spawnZBoss("SZ");
			return true;
		}
		if (args[0].equals("SZ")) {
			spawnZBoss("TZ");
			return true;
		}
		if (args[0].equals("TZ")) {
			spawnZBoss("UZ");
			return true;
		}
		if (args[0].equals("UZ")) {
			spawnZBoss("VZ");
			return true;
		}
		if (args[0].equals("VZ")) {
			spawnZBoss("WZ");
			return true;
		}
		if (args[0].equals("WZ")) {
			spawnZBoss("XZ");
			return true;
		}
		if (args[0].equals("XZ")) {
			spawnZBoss("YZ");
			return true;
		}
		if (args[0].equals("Z")) {
			Player p = Bukkit.getPlayer(UUID.fromString(dungeon.getString("dungeons." + args[0])));
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "lp user " + p.getName() + " parent add prestige");
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "sudo " + p.getName() + " prestige");
			p.sendMessage(utils.color("&6Rankup &8> &fWell done! You have prestiged!"));
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "spawn " + p.getName());
			dungeon.set("dungeons." + args[0], null);
			try {
				dungeon.save(dungeons);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			return true;
		}
		
		Player p = Bukkit.getPlayer(UUID.fromString(dungeon.getString("dungeons." + args[0])));
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "ezadmin forcerankup " + p.getName());
		p.sendMessage(utils.color("&6Rankup &8> &fWell done! You have ranked up!"));
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "spawn " + p.getName());
			}
		}, 40L);
		dungeon.set("dungeons." + args[0], null);
		try {
			dungeon.save(dungeons);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		if (args[0].equals("A")) {
			p.sendMessage("");
			p.sendMessage("");
			p.sendMessage("");
			p.sendMessage(utils.color("&6&nYou have killed the first boss!"));
			p.sendMessage("");
			p.sendMessage(utils.color("&fYou received a boss drop named &cOld Flesh&f."));
			p.sendMessage(utils.color("&fYou can &f&ncraft boss drops&f together to get &f&ncustom items&f!"));
			p.sendMessage("");
			p.sendMessage("");
			p.sendMessage("");
		}
		return true;
		
	}
	
	
	private void spawnZBoss(String text) {
		File data = new File("plugins/Dungeons/", "locations.yml");
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
		String worldBoss = cfg.getString("location.AZBOSS.world");
		double xBoss = cfg.getDouble("location.AZBOSS.x");
		double yBoss = cfg.getDouble("location.AZBOSS.y");
		double zBoss = cfg.getDouble("location.AZBOSS.z");
		Location bossLoc = new Location(Bukkit.getWorld(worldBoss), xBoss, yBoss, zBoss);
		try {
			mm.spawnMythicMob(text, bossLoc, 1);
		} catch (InvalidMobTypeException e) {
			e.printStackTrace();
		}
	}

}
