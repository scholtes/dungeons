package me.scholtes.dungeons.commands;

import java.io.File;
import java.io.IOException;
import java.util.regex.Pattern;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import me.scholtes.dungeons.Utils;

public class DeleteLocationCMD implements CommandExecutor {

    private Utils utils;
	
	public DeleteLocationCMD(Utils utils) {
		this.utils = utils;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		File data = new File("plugins/Dungeons/", "locations.yml");
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
		
		if (!(sender instanceof Player)) {
			return true;
		}
		
		if (!sender.isOp()) {
			sender.sendMessage(utils.color("&cYou cannot do this!"));
			return true;
		}
		if (args.length == 0) {
			sender.sendMessage(utils.color("&cCorrect usage: /deletelocation <dungeon>"));
			return true;
		}
		if (!Pattern.matches("^[a-zA-Z]*$", args[0].toUpperCase())) {
			sender.sendMessage(utils.color("&cPlease specify a letter from A to Z"));
			return true;
		}
		sender.sendMessage(utils.color("&aLocation deleted"));
		cfg.set("location." + args[0].toUpperCase(), null);
		try {
			cfg.save(data);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return true;
	}

}
