package me.scholtes.dungeons.commands;

import java.io.File;
import java.io.IOException;
import java.util.regex.Pattern;

import org.apache.commons.lang3.math.NumberUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import io.lumine.xikage.mythicmobs.MythicMobs;
import io.lumine.xikage.mythicmobs.api.bukkit.BukkitAPIHelper;
import io.lumine.xikage.mythicmobs.api.exceptions.InvalidMobTypeException;
import me.scholtes.dungeons.Dungeons;
import me.scholtes.dungeons.Utils;

public class TeleportCMD implements CommandExecutor {
	
	private Utils utils;
	private Dungeons plugin;
	private BukkitAPIHelper mm = MythicMobs.inst().getAPIHelper();
	
	public TeleportCMD(Utils utils, Dungeons plugin) {
		this.utils = utils;
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		File data = new File("plugins/Dungeons/", "locations.yml");
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
		
		File dungeons = new File("plugins/Dungeons/", "dungeons.yml");
		FileConfiguration dungeon = YamlConfiguration.loadConfiguration(dungeons);
		
		if (!sender.isOp()) {
			sender.sendMessage(utils.color("&cYou cannot do this!"));
			return true;
		}
		if (args.length == 0 || args.length == 1 || args.length == 2) {
			sender.sendMessage(utils.color("&cCorrect usage: /tpdungeon <dungeon> <player> <money>"));
			return true;
		}
		if (!Pattern.matches("^[a-zA-Z]*$", args[0].toUpperCase())) {
			sender.sendMessage(utils.color("&cPlease specify a letter from A to Z"));
			return true;
		}
		if (Bukkit.getPlayer(args[1]) == null || !Bukkit.getPlayer(args[1]).isOnline()) {
			sender.sendMessage(utils.color("&cThat's not a valid player!"));
			return true;
		}
		if (!NumberUtils.isParsable(args[2])) {
			sender.sendMessage(utils.color("&cPlease only input a number!"));
			return true;
		}
		boolean isDungeon = false;
		for (String key : cfg.getConfigurationSection("location").getKeys(false)) {
			if (key.equals(args[0])) {
				isDungeon = true;
				break;
			}
		}
		
		if (!isDungeon) {
			sender.sendMessage(utils.color("&cThat isn't a valid dungeon!"));
			return true;
		}
		
		if (!Bukkit.getPlayer(args[1]).hasPermission("dungeon." + args[0].toUpperCase())) {
			Bukkit.getPlayer(args[1]).sendMessage(utils.color("&6Dungeon Keeper &8> &fYou can only fight the boss at your rank!"));;
			return true;
		}
		
		
		if (dungeon.getConfigurationSection("dungeons") != null) {
			boolean isInUse = false;
			for (String key : dungeon.getConfigurationSection("dungeons").getKeys(false)) {
				if (key.equals(args[0])) {
					isInUse = true;
					break;
				}
			}
			if (isInUse) {
				Bukkit.getPlayer(args[1]).sendMessage(utils.color("&6Dungeon Keeper &8> &fThe dungeon is currently in use!"));;
				return true;
			}
		}
		
		double x = cfg.getDouble("location." + args[0].toUpperCase() + ".x");
		double y = cfg.getDouble("location." + args[0].toUpperCase() + ".y");
		double z = cfg.getDouble("location." + args[0].toUpperCase() + ".z");
		float yaw = (float) cfg.getDouble("location." + args[0].toUpperCase() + ".yaw");
		float pitch = (float) cfg.getDouble("location." + args[0].toUpperCase() + ".pitch");
		Location location = new Location(Bukkit.getWorld(cfg.getString("location." + args[0].toUpperCase() + ".world")), x, y, z, yaw, pitch);
		
		Bukkit.getPlayer(args[1]).teleport(location);
		Bukkit.getPlayer(args[1]).sendMessage(utils.color("&6Dungeon Keeper &8> &fYou are now entering dungeon " + args[0].toUpperCase()));;
	
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "eco take " + Bukkit.getPlayer(args[1]).getName() + " " + args[2]);

		dungeon.set("dungeons." + args[0], Bukkit.getPlayer(args[1]).getUniqueId().toString());
		try {
			dungeon.save(dungeons);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				Bukkit.getPlayer(args[1]).sendMessage(utils.color("&6Dungeon Keeper &8> &fThe boss has appeared!"));
				if (args[0].equals("Z")) {
					spawnBoss("AZ");
				} else {
					spawnBoss(args[0]);
				}
			}
		}, 70L);
	
	
		return false;
	}
	
	private void spawnBoss(String text) {
		File data = new File("plugins/Dungeons/", "locations.yml");
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
		String worldBoss = cfg.getString("location." + text + "BOSS.world");
		double xBoss = cfg.getDouble("location." + text + "BOSS.x");
		double yBoss = cfg.getDouble("location." + text + "BOSS.y");
		double zBoss = cfg.getDouble("location." + text + "BOSS.z");
		Location bossLoc = new Location(Bukkit.getWorld(worldBoss), xBoss, yBoss, zBoss);
		try {
			mm.spawnMythicMob(text, bossLoc, 1);
		} catch (InvalidMobTypeException e) {
			e.printStackTrace();
		}
	}

}
