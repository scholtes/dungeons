package me.scholtes.dungeons.events;

import java.io.File;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import me.scholtes.dungeons.Utils;

public class PreCommandEvent implements Listener {
	
	private Utils utils;
	
	public PreCommandEvent(Utils utils) {
		this.utils = utils;
	}
	
	@EventHandler
	public void preProcess(PlayerCommandPreprocessEvent e) {
		if (e.getPlayer().isOp()) {
			return;
		}
		File dungeons = new File("plugins/Dungeons/", "dungeons.yml");
		FileConfiguration dungeon = YamlConfiguration.loadConfiguration(dungeons);
		for (String key : dungeon.getConfigurationSection("dungeons").getKeys(false)) {
			if (Bukkit.getPlayer(UUID.fromString(dungeon.getString("dungeons." + key))) == e.getPlayer()) {
				e.getPlayer().sendMessage(utils.color("&6Dungeon Keeper &8> &fYou can't run commands while in a dungeon"));
				e.setCancelled(true);
				break;
			}
		}
	}

}
