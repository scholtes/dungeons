package me.scholtes.dungeons.events;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

public class DeathEvent implements Listener {
	
	@EventHandler
	public void onDeath(EntityDeathEvent e) {
		if (!(e.getEntity() instanceof Player)) {
			return;
		}
		Player p = (Player) e.getEntity();
		p.setExp(e.getDroppedExp());
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "spawn " + p.getName());
	    
	    File dungeons = new File("plugins/Dungeons/", "dungeons.yml");
		FileConfiguration dungeon = YamlConfiguration.loadConfiguration(dungeons);
		
		for (String key : dungeon.getConfigurationSection("dungeons").getKeys(false)) {
			if (Bukkit.getPlayer(UUID.fromString(dungeon.getString("dungeons." + key))) == p) {
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "mm m kill " + key);
				dungeon.set("dungeons." + key, null);
				try {
					dungeon.save(dungeons);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				break;
			}
		}
	    
	}
	

}
