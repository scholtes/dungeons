package me.scholtes.dungeons.events;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class DisconnectEvent implements Listener {
	
	@EventHandler
	public void onDisconnect(PlayerQuitEvent e) {
		File dungeons = new File("plugins/Dungeons/", "dungeons.yml");
		FileConfiguration dungeon = YamlConfiguration.loadConfiguration(dungeons);
		Player p = e.getPlayer();
		for (String key : dungeon.getConfigurationSection("dungeons").getKeys(false)) {
			if (Bukkit.getPlayer(UUID.fromString(dungeon.getString("dungeons." + key))) == p) {
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "spawn " + p.getName());
				dungeon.set("dungeons." + key, null);
				try {
					dungeon.save(dungeons);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				List<Entity> entities = p.getNearbyEntities(15, 15, 15);
				
				for (Entity entity : entities) {
					if (entity instanceof LivingEntity) {
						LivingEntity livingEntity = (LivingEntity) entity;
						livingEntity.setHealth(0);
					}
				}
				break;
			}
		}
		
	}

}
