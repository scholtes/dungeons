package me.scholtes.dungeons;

import org.bukkit.plugin.java.JavaPlugin;

import me.scholtes.dungeons.commands.BossKillCMD;
import me.scholtes.dungeons.commands.DeleteLocationCMD;
import me.scholtes.dungeons.commands.SetLocationCMD;
import me.scholtes.dungeons.commands.TeleportCMD;
import me.scholtes.dungeons.events.DeathEvent;
import me.scholtes.dungeons.events.DisconnectEvent;
import me.scholtes.dungeons.events.PreCommandEvent;

public class Dungeons extends JavaPlugin {
	
	private Utils utils = new Utils();
	
	public void onEnable() {
		
		getCommand("setlocation").setExecutor(new SetLocationCMD(utils));
		getCommand("deletelocation").setExecutor(new DeleteLocationCMD(utils));
		getCommand("tpdungeon").setExecutor(new TeleportCMD(utils, this));
		getCommand("dungeonkill").setExecutor(new BossKillCMD(utils, this));
		
		getServer().getPluginManager().registerEvents(new DeathEvent(), this);
		getServer().getPluginManager().registerEvents(new DisconnectEvent(), this);
		getServer().getPluginManager().registerEvents(new PreCommandEvent(utils), this);
		
	}

}
